import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.util.LongAccumulator;

import scala.Tuple2;
import utils.PorterStemmer;

public class MyIndexer {

	public static void main(final String[] args) {
		final JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("MyIndexer"));

		final String stopwordFilePath = "src/main/resources/stopword-list.txt";
		
		// Getting all the stop words as a set
		Set<String> stopwords = new HashSet<>();
		try {
			final File stopwordFile = new File(stopwordFilePath);
			Scanner scanner = new Scanner(stopwordFile);
			while (scanner.hasNextLine())
			{
				stopwords.add(scanner.nextLine());
			}
			scanner.close();
		} catch (Exception e) {
			System.exit(1);
		}
		
		Broadcast<Set<String>> stopwordsVar = sc.broadcast(stopwords);

		// accumulator for num of documents and sum of documents
		LongAccumulator numOfDocuments = sc.sc().longAccumulator("Number of Documents");
		LongAccumulator sumOfDocumentLengths = sc.sc().longAccumulator("Sum of Document Lengths");

		// setting the record delimiter to the start of articles
		sc.hadoopConfiguration().set("textinputformat.record.delimiter", "\n[[");

		JavaRDD<String> _sc = sc.textFile(args[0]).repartition(500);
		JavaPairRDD<String, String> intermediate = _sc.flatMapToPair(s -> {

			// Contains the pairs that we want to output
			List<Tuple2<String, String>> pairs = new LinkedList<>();

			PorterStemmer stemmer = new PorterStemmer();

			// counter for number of terms in a document
			Integer documentLength = 0;

			// map containing term frequencies for a document
			Map<String, Integer> termFrequencies = new HashMap<>();

			// getting the title (need to replace "[[" for the first article entry)
			String title = s.substring(0, s.indexOf("]]")).replace("[[", "").toLowerCase();

			StringTokenizer tokenizer = new StringTokenizer(s.substring(s.indexOf("]]")));

			while (tokenizer.hasMoreTokens())
			{
				// cleaning up the junk in the strings and converting to lower case
				String token = tokenizer.nextToken().replaceAll("[^a-zA-Z0-9_-]", "").toLowerCase();

				// if the token is not a stop word and not empty then stem it
				if (!token.isEmpty() && !stopwordsVar.value().contains(token)) {
					token = stemmer.stem(token);
					
					documentLength++;

					termFrequencies.put(token, termFrequencies.getOrDefault(token, 0) + 1);
				}
			}

			// adding the document length pair (with value tag 'L' for length)
			pairs.add(new Tuple2<String, String>(
				title+"L", documentLength.toString())
			);

			// adding the term frequency (with value tag 'F' for frequency) and adding the frequency to the key
			// so that it can be sorted by a secondary sort 
			termFrequencies.forEach((term, frequency) -> 
				pairs.add(new Tuple2<String, String>(
					term+"F"+frequency, title)
				)
			);	

			// increment the number of documents
			numOfDocuments.add(1);

			// add the document length to the sum
			sumOfDocumentLengths.add(documentLength);

			return pairs.iterator();
		}); 

		// outputting the document lengths to a file
		intermediate
		.filter(p -> p._1().charAt(p._1().length()-1) == 'L')
		.sortByKey()
		// getting rid of the tag
		.map(p -> new Tuple2<String, String>(p._1().substring(0, p._1().length()-1), p._2()))
        .saveAsTextFile(args[1] + "/documentLengths");

		// outputting the term frequencies and number of documents containing the term
		intermediate
		.filter(p -> p._1().contains("F"))
		// sorting by term and frequency
		.sortByKey(new TFSorter())
		// reorganising the data to be (term, (title, term frequency))
		.mapToPair(p -> new Tuple2<String, Tuple2<String, Integer>>(
			p._1().substring(0, p._1().indexOf('F')),
			new Tuple2<String, Integer>(p._2(), Integer.parseInt(p._1().substring(p._1().indexOf('F')+1))
		)))
		.saveAsTextFile(args[1] + "/termFrequencies");

		List<Tuple2<String, Double>> miscInfo = new LinkedList<>();
		miscInfo.add(new Tuple2<String, Double>("Number of Documents", (double) numOfDocuments.value()));
		miscInfo.add(new Tuple2<String, Double>("Average Document Length", (double) sumOfDocumentLengths.value() / numOfDocuments.value()));

		sc.parallelize(miscInfo)
		.saveAsTextFile(args[1] + "/miscInfo");

		sc.close();
	}
}


class TFSorter implements Comparator<String>, Serializable {

	private static final long serialVersionUID = -1222145010145158976L;

	@Override
	public int compare(String s1, String s2) {
		int indexS1 = s1.indexOf('F'), indexS2 = s2.indexOf('F');
		int termComparison = s1.substring(0, indexS1).compareTo(s2.substring(0, indexS2));

		// if the terms are the same compare on frequency (descending order), otherwise return the term comparison
		if (termComparison == 0) {
			return Integer.parseInt(s2.substring(indexS2+1)) - Integer.parseInt(s1.substring(indexS1+1));
		} else {
			return termComparison;
		}
	}
}
