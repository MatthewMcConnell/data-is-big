# Data is Big

This is a small coursework project that takes in wikipedia articles and ranks them using the Okapi BM25 ranking function. We used Spark and Java to implement this in our `MyIndexer.java` file.

This project was designed and worked on by a team, of which the members were:
- Matthew McConnell
- Olga Jodełka
- Gabor Mihucz
